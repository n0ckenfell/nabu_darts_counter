package DartsCounter;

import java.util.ArrayList;
import java.util.List;

public class DartsMatch {

    // SETUP

    private static final int GAME_START_SCORE = 501;
    private static final int DARTS_PER_RECORD = 3;


    private final Player playerOne;
    private List<Throw> playerOneLog = new ArrayList<>();
    
    private final Player playerTwo;
    private List<Throw> playerTwoLog = new ArrayList<>();

    private Player activePlayer;


    // CONSTRUCTOR

    public DartsMatch(Player playerOne, Player playerTwo) {
        this.playerOne = playerOne;
        this.playerTwo = playerTwo;
        this.activePlayer = playerOne;
    }

    // API

    public String title() {
        return String.format("%s vs %s", playerOne.name(), playerTwo.name());
    }


    public int[] score() {
        return new int[]{playerOneScore(), playerTwoScore()};
    }

    private int playerOneScore() {
        return GAME_START_SCORE - playerOneLog.stream().mapToInt(t -> t.value * t.multiplier).sum();
    }

    private int playerTwoScore() {
        return GAME_START_SCORE - playerTwoLog.stream().mapToInt(t -> t.value * t.multiplier).sum();
    }

    public void recordThrow(int value, int multiplier) {
        if (isValidDart(value,multiplier)) {
            activeThrowLog().add(new Throw(value, multiplier));
            // dectectWin()
        }
        detectPlayerChange();
    }

    private boolean isValidDart(int value, int multiplier) {
        return (activeLogSum() - value * multiplier >= 0);
    }

    public int activePlayerScore() {
        return GAME_START_SCORE - activeLogSum();
    }

    public String activePlayerName() {
        return activePlayer.name();
    }

    public Player winner() {
        if (playerOneScore() == 0)
            return playerOne;
        else if (playerTwoScore() == 0)
            return playerTwo;

        return null;
    }

    private List<Throw> activeThrowLog() {
        return activePlayer == playerOne ? playerOneLog : playerTwoLog;
    }


    // DETAILS

    private void detectPlayerChange() {
        int throwCount = activeThrowLog().size() % DARTS_PER_RECORD;
        if (throwCount == 0)
            changePlayer();
    }

    private void changePlayer() {
        activePlayer = activePlayer == playerOne ? playerTwo : playerOne;
    }

    private int activeLogSum() {
        return activeThrowLog().stream().mapToInt(t -> t.value * t.multiplier).sum();
    }


    private class Throw {
        public final int value;
        public final int multiplier;

        public Throw(int value, int multiplier) {
            this.value = value;
            this.multiplier = multiplier;
        }

    }

}
