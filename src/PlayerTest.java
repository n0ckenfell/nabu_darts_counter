import DartsCounter.DartsMatch;
import DartsCounter.Player;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlayerTest {

    DartsMatch match;
    private Player bob;
    private Player frank;

    @Before
    public void initializeGame() {
        bob = new Player("Bob");
        frank = new Player("Frank");
        match = new DartsMatch(bob,frank);
    }

    @Test
    public void aGamesTitleShowsTheTwoPlayers() {
        assertEquals( "Bob vs Frank", match.title() );
    }


    @Test
    public void gameStartsWith501() {
        int[] expected = new int[]{501,501};

        assertArrayEquals( expected, match.score() );

    }

    @Test
    public void playerOneIsFirstActivePlayer() {
        assertEquals( "Bob", match.activePlayerName() );
    }

    @Test
    public void activePlayerThrowsFirstDart() {
        match.recordThrow(20,3);
        assertEquals(441, match.activePlayerScore());
    }

    @Test
    public void activePlayerThrowsSecondDart() {
        match.recordThrow(20,3);
        match.recordThrow(10,1);
        assertEquals(431, match.activePlayerScore());
    }

    @Test
    public void afterThreeThrowsPlayerTwoIsActivePlayer() {
        match.recordThrow(20,3);
        match.recordThrow(10,1);
        match.recordThrow(15,2);
        assertEquals( "Frank", match.activePlayerName() );
    }

    @Test
    public void secondPlayerThrowsDarts() {
        // Given: 1st player done
        match.recordThrow(20,3);
        match.recordThrow(10,1);
        match.recordThrow(15,2);

        // When: 2nd player throws
        match.recordThrow(1,1);
        assertEquals(500, match.activePlayerScore());
    }

    @Test
    public void winnerOfANewGameIsNull() {
        assertNull(match.winner());
    }


    @Test
    public void detectWinnerWhen1stPlayerThrows9Darter() {
        // Given a new Game

        // When first player throws a 9-darter
        throw180();         // Bob
        noScoreThrow();     // Frank
        throw180();         // Bob
        noScoreThrow();     // Frank
        nineDartFinish();   // Bob

        // Then 1st player is the winner with 0 rest
        int[] expected = new int[]{0,501};
        assertArrayEquals( expected, match.score() );
        assertEquals(bob, match.winner());
    }

    @Test
    public void detectWinnerWhen2ndPlayerThrows9Darter() {
        // Given a new Game

        // When first player throws a 9-darter
        noScoreThrow();     // Bob
        throw180();         // Frank
        noScoreThrow();     // Bob
        throw180();         // Frank
        noScoreThrow();     // Bob
        nineDartFinish();   // Frank

        // Then 1st player is the winner with 0 rest
        int[] expected = new int[]{501,0};
        assertArrayEquals( expected, match.score() );
        assertEquals(frank, match.winner());
    }

    @Test
    public void overthrowIsNoScore() {
        // Given a new Game

        // When first player throws a 9-darter
        throw180();         // Bob
        noScoreThrow();     // Frank
        throw180();         // Bob
        noScoreThrow();     // Frank
        throw180();         // Bob

        int[] expected = new int[]{21,501};
        assertArrayEquals( expected, match.score() );

        assertEquals(frank.name(), match.activePlayerName());
    }





    private void nineDartFinish() {
        match.recordThrow(20,3);
        match.recordThrow(19,3);
        match.recordThrow(12,2);
    }

    private void throw180() {
        match.recordThrow(20,3);
        match.recordThrow(20,3);
        match.recordThrow(20,3);
    }

    private void noScoreThrow() {
        match.recordThrow(0,0);
        match.recordThrow(0,0);
        match.recordThrow(0,0);
    }

}
